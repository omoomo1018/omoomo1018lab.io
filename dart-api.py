from urllib.request import urlopen
from io import BytesIO
from zipfile import ZipFile
import xml.etree.ElementTree as ET
import sqlite3
import json

def create_tables():
    conn = sqlite3.connect("dart-fss.db")
    conn.execute('CREATE TABLE DART_FSS(CORP_CODES TEXT, CORP_NAME TEXT, CORP_ADRS TEXT)')
    conn.commit()
    conn.close()

def alter_table():
    conn = sqlite3.connect("dart-fss.db")
    conn.execute('ALTER TABLE DART_FSS ADD COLUMN JBG TEXT')
    conn.commit()
    conn.close()

def delete_table():
    conn = sqlite3.connect("dart-fss.db")
    conn.execute('DELETE FROM DART_FSS WHERE MCA IS NULL')
    conn.commit()
    conn.close()


def drop_table():
    conn = sqlite3.connect("dart-fss.db")
    conn.execute('DROP TABLE DART_FSS WHERE ')
    conn.commit()
    conn.close()

def select_table():
    conn = sqlite3.connect("dart-fss.db")
    cur = conn.cursor()
    cur.execute("""SELECT * FROM DART_FSS WHERE CORP_ADRS like '%시흥%' or CORP_ADRS like '%안산%' or CORP_ADRS like '%인천%' or CORP_ADRS like '%서울%' or CORP_ADRS like '%광명%'""")
    #cur.execute("""SELECT count(*) FROM DART_FSS""")
    sql2tup = cur.fetchall()
    tup2lst = []
    for tup in sql2tup:
        tup2lst.append(list(tup))
    conn.close()

    return tup2lst

def bind_params(params: dict):
    url_params = []
    for key in params:
        url_params.append(key + '=' + params[key])
    return url_params

def update_all_corp_code():
    api_key = '3bd941b36aea491d45d41f79aa7acf411be2f2d3'
    url = 'https://opendart.fss.or.kr/api/corpCode.xml?crtfc_key=' + api_key

    with urlopen(url) as zipresp:
        with ZipFile(BytesIO(zipresp.read())) as zfile:
            zfile.extractall('corp_num')

    tree = ET.parse('./corp_num/CORPCODE.xml')

    corp_codes = []
    for lst in tree.findall('./list'):
        if len(lst.find('stock_code').text) == 6:
            print(lst.find('corp_code').text)
            corp_codes.append(lst.find('corp_code').text)
    aa = len(lst.find('stock_code').text)

    conn = sqlite3.connect("dart-fss.db")
    cur = conn.cursor()
    conn.execute('DELETE FROM DART_FSS')
    cnt = 0
    for corp_code in corp_codes:
        jsonurl = 'https://opendart.fss.or.kr/api/company.json?'
        params = {
            'crtfc_key': api_key,  # API 인증키
            'corp_code': corp_code  # 기업 고유번호 8자리
        }
        jsonurl = jsonurl + '&'.join(bind_params(params))

        resp = urlopen(jsonurl)

        if resp.code == 200:
            resp_json_str = resp.read().decode()
            resp_json = json.loads(resp_json_str)
            #if resp_json['corp_cls'] == 'E':
            sql_query = 'INSERT INTO DART_FSS VALUES("' + corp_code + '", "' + resp_json['corp_name'] + '", "' + resp_json['adres'] + '")'
            conn.execute(sql_query)
            cnt = cnt + 1
            print(str(cnt) + '/' + str(aa) + ' : ' + sql_query + ' / ' + resp_json['corp_cls'])

        if (cnt == 2000):
            break

    conn.commit()
    conn.close()


def lst_1st_col_out():
    lst = select_table()
    print(len(lst))
    retlst = []
    for item in lst:
        retlst.append([item[1], item[2]])

    return retlst

#update_all_corp_code()
#create_tables()
#print(lst_1st_col_out())
#print(select_table())


def jaemu_jungbo():
    corp_list = select_table()

    api_key = '3bd941b36aea491d45d41f79aa7acf411be2f2d3'
    cnt = 0
    conn = sqlite3.connect("dart-fss.db")

    for corp_code in corp_list:
        url = '	https://opendart.fss.or.kr/api/fnlttSinglAcnt.json?crtfc_key=' + api_key + '&corp_code=' + corp_code[0] + '&bsns_year=2018&reprt_code=11011'
        print(url)
        resp = urlopen(url)
        if resp.code == 200:
            resp_json_str = resp.read().decode()
            resp_json = json.loads(resp_json_str)
            status = resp_json['status']
            message = resp_json['message']
            if status == '000':
                cnt = cnt + 1
                detail = resp_json['list']
                mca = yeii = dgsii = jscg = bccg = jbg = 0
                for x in detail:
                    #if x['fs_div'] == 'OFS' and x['account_nm'] == '매출액':
                    #    mca = x['thstrm_amount']
                    #if x['fs_div'] == 'OFS' and x['account_nm'] == '영업이익':
                    #    yeii = x['thstrm_amount']
                    #if x['fs_div'] == 'OFS' and x['account_nm'] == '당기순이익':
                    #    dgsii = x['thstrm_amount']
                    #if x['fs_div'] == 'OFS' and x['account_nm'] == '자산총계':
                    #    jscg = x['thstrm_amount']
                    #if x['fs_div'] == 'OFS' and x['account_nm'] == '부채총계':
                    #    bccg = x['thstrm_amount']
                    if x['fs_div'] == 'OFS' and x['account_nm'] == '자본총계':
                        jbg = x['thstrm_amount']
                sql_query = 'UPDATE DART_FSS SET JBG = "' + str(jbg) + '"WHERE CORP_CODES = "' + corp_code[0] + '"'
                conn.execute(sql_query)
            else:
                print(corp_code[0] + ' ' + message)

    conn.commit()
    conn.close()
    print(cnt)

#alter_table()
#delete_table()
print(select_table())
#jaemu_jungbo()
